# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Elkana Bardugo <ttv200@gmail.com>, 2016.
# Elkana Bardugo <ttv200@gmail.com>, 2017. #zanata
msgid ""
msgstr ""
"Project-Id-Version: konsoleprofiles\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-05 03:09+0000\n"
"PO-Revision-Date: 2017-05-16 06:53-0400\n"
"Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>\n"
"Language-Team: Hebrew <kde-i18n-doc@kde.org>\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Zanata 3.9.6\n"

#: package/contents/ui/konsoleprofiles.qml:62
#, fuzzy, kde-format
#| msgid "Konsole Profiles"
msgctxt "@title"
msgid "Konsole Profiles"
msgstr "פרופילי טרמינל Konsole"

#: package/contents/ui/konsoleprofiles.qml:82
#, kde-format
msgid "Arbitrary String Which Says Something"
msgstr "מחרוזת שרירותית שאומרת משהו"
