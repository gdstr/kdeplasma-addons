# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kdeplasma-addons package.
#
# Mincho Kondarev <mkondarev@yahoo.de>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: kdeplasma-addons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-01 02:33+0000\n"
"PO-Revision-Date: 2023-03-17 21:49+0100\n"
"Last-Translator: Mincho Kondarev <mkondarev@yahoo.de>\n"
"Language-Team: Bulgarian <kde-i18n-doc@kde.org>\n"
"Language: bg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.12.3\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: spellcheck.cpp:98 spellcheck_config.cpp:65 spellcheck_config.cpp:96
#, kde-format
msgid "spell"
msgstr "проверка на правописа"

#: spellcheck.cpp:103
#, kde-format
msgctxt ""
"Spelling checking runner syntax, first word is trigger word, e.g.  \"spell\"."
msgid "%1:q:"
msgstr "%1:q:"

#: spellcheck.cpp:107
#, kde-format
msgid "Checks the spelling of :q:."
msgstr "Проверява правописа на :q :."

#: spellcheck.cpp:223
#, kde-format
msgctxt "Term is spelled correctly"
msgid "Correct"
msgstr "Правилно"

#: spellcheck.cpp:232
#, kde-format
msgid "Suggested term"
msgstr "Предложение"

#: spellcheck.cpp:262
#, kde-kuit-format
msgctxt "@info"
msgid ""
"No dictionary found. Please install <resource>hunspell</resource> package "
"using your package manager"
msgstr ""
"Не е намерен речник, моля инсталирайте  пакета <resource>hunspell</resource> "
"през системния пакетен мениджър"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox)
#: spellcheck_config.ui:17
#, kde-format
msgid "Spell Check Settings"
msgstr "Настройки за проверка на правописа"

#. i18n: ectx: property (text), widget (QCheckBox, m_requireTriggerWord)
#: spellcheck_config.ui:23
#, kde-format
msgid "&Require trigger word"
msgstr "&Изискване на задействаща дума"

#. i18n: ectx: property (text), widget (QLabel, label)
#: spellcheck_config.ui:32
#, kde-format
msgid "&Trigger word:"
msgstr "&Задействаща дума:"

#. i18n: ectx: property (text), widget (QPushButton, m_openKcmButton)
#: spellcheck_config.ui:62
#, kde-format
msgid "Configure Dictionaries…"
msgstr "Конфигуриране на речници…"
