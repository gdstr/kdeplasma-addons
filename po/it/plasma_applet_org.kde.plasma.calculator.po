# translation of plasma_applet_calculator.po to Italian
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Federico Zenith <zenith@chemeng.ntnu.no>, 2008.
# Federico Zenith <federico.zenith@member.fsf.org>, 2009.
# Vincenzo Reale <smart2128vr@gmail.com>, 2010, 2012, 2022, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_calculator\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-08 02:24+0000\n"
"PO-Revision-Date: 2023-02-25 00:11+0100\n"
"Last-Translator: Vincenzo Reale <smart2128vr@gmail.com>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.12.2\n"

#: package/contents/ui/calculator.qml:291
#, kde-format
msgctxt ""
"Abbreviation for result (undefined) of division by zero, max. six to nine "
"characters."
msgid "undef"
msgstr "non def"

#: package/contents/ui/calculator.qml:331
#, kde-format
msgctxt "@label calculation result"
msgid "Result"
msgstr "Risultato"

#: package/contents/ui/calculator.qml:354
#, kde-format
msgctxt "Text of the clear button"
msgid "C"
msgstr "C"

#: package/contents/ui/calculator.qml:367
#, kde-format
msgctxt "Text of the division button"
msgid "÷"
msgstr "÷"

#: package/contents/ui/calculator.qml:380
#, kde-format
msgctxt "Text of the multiplication button"
msgid "×"
msgstr "×"

#: package/contents/ui/calculator.qml:392
#, kde-format
msgctxt "Text of the all clear button"
msgid "AC"
msgstr "AC"

#: package/contents/ui/calculator.qml:444
#, kde-format
msgctxt "Text of the minus button"
msgid "-"
msgstr "-"

#: package/contents/ui/calculator.qml:496
#, kde-format
msgctxt "Text of the plus button"
msgid "+"
msgstr "+"

#: package/contents/ui/calculator.qml:547
#, kde-format
msgctxt "Text of the equals button"
msgid "="
msgstr "="

#~ msgctxt "The − button of the calculator"
#~ msgid "−"
#~ msgstr "−"

#~ msgid "ERROR"
#~ msgstr "ERRORE"

#~ msgid "ERROR: DIV BY 0"
#~ msgstr "ERRORE: DIV. PER 0"

#~ msgctxt "The ∕ button of the calculator"
#~ msgid "∕"
#~ msgstr "∕"

#~ msgid "Copy"
#~ msgstr "Copia"

#~ msgid "Paste"
#~ msgstr "Incolla"
