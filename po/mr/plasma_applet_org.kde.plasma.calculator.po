# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Chetan Khona <chetan@kompkin.com>, 2013.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-08 02:24+0000\n"
"PO-Revision-Date: 2013-03-20 17:06+0530\n"
"Last-Translator: Chetan Khona <chetan@kompkin.com>\n"
"Language-Team: Marathi <kde-i18n-doc@kde.org>\n"
"Language: mr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"X-Generator: Lokalize 1.5\n"

#: package/contents/ui/calculator.qml:291
#, kde-format
msgctxt ""
"Abbreviation for result (undefined) of division by zero, max. six to nine "
"characters."
msgid "undef"
msgstr ""

#: package/contents/ui/calculator.qml:331
#, kde-format
msgctxt "@label calculation result"
msgid "Result"
msgstr ""

#: package/contents/ui/calculator.qml:354
#, kde-format
msgctxt "Text of the clear button"
msgid "C"
msgstr "C"

#: package/contents/ui/calculator.qml:367
#, kde-format
msgctxt "Text of the division button"
msgid "÷"
msgstr "÷"

#: package/contents/ui/calculator.qml:380
#, kde-format
msgctxt "Text of the multiplication button"
msgid "×"
msgstr "×"

#: package/contents/ui/calculator.qml:392
#, kde-format
msgctxt "Text of the all clear button"
msgid "AC"
msgstr "AC"

#: package/contents/ui/calculator.qml:444
#, kde-format
msgctxt "Text of the minus button"
msgid "-"
msgstr "-"

#: package/contents/ui/calculator.qml:496
#, kde-format
msgctxt "Text of the plus button"
msgid "+"
msgstr "+"

#: package/contents/ui/calculator.qml:547
#, kde-format
msgctxt "Text of the equals button"
msgid "="
msgstr "="
