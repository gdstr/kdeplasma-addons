msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_org\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-15 00:46+0000\n"
"PO-Revision-Date: 2022-03-25 09:53+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: package/contents/config/config.qml:12
#, kde-format
msgctxt "@title"
msgid "General"
msgstr "Geral"

#: package/contents/ui/ConfigGeneral.qml:30
#, kde-format
msgctxt "@label:spinbox"
msgid "Maximum columns:"
msgstr "Máximo de colunas:"

#: package/contents/ui/ConfigGeneral.qml:30
#, kde-format
msgctxt "@label:spinbox"
msgid "Maximum rows:"
msgstr "Máximo de linhas:"

#: package/contents/ui/ConfigGeneral.qml:44
#, kde-format
msgctxt "@title:group"
msgid "Appearance:"
msgstr "Aparência:"

#: package/contents/ui/ConfigGeneral.qml:46
#, kde-format
msgctxt "@option:check"
msgid "Show launcher names"
msgstr "Mostrar os nomes dos lançamentos"

#: package/contents/ui/ConfigGeneral.qml:51
#, kde-format
msgctxt "@option:check"
msgid "Enable popup"
msgstr "Activar a mensagem"

#: package/contents/ui/ConfigGeneral.qml:61
#, kde-format
msgctxt "@title:group"
msgid "Title:"
msgstr "Título:"

#: package/contents/ui/ConfigGeneral.qml:69
#, kde-format
msgctxt "@option:check"
msgid "Show:"
msgstr "Mostrar:"

#: package/contents/ui/ConfigGeneral.qml:85
#, kde-format
msgctxt "@info:placeholder"
msgid "Custom title"
msgstr "Título personalizado"

#: package/contents/ui/IconItem.qml:175
#, kde-format
msgid "Launch %1"
msgstr "Lançar o %1"

#: package/contents/ui/IconItem.qml:259
#, kde-format
msgctxt "@action:inmenu"
msgid "Add Launcher…"
msgstr "Adicionar um Lançamento…"

#: package/contents/ui/IconItem.qml:265
#, kde-format
msgctxt "@action:inmenu"
msgid "Edit Launcher…"
msgstr "Editar o Lançamento…"

#: package/contents/ui/IconItem.qml:271
#, kde-format
msgctxt "@action:inmenu"
msgid "Remove Launcher"
msgstr "Remover o Lançamento"

#: package/contents/ui/main.qml:142
#, kde-format
msgid "Quicklaunch"
msgstr "Lançamento Rápido"

#: package/contents/ui/main.qml:143
#, kde-format
msgctxt "@info"
msgid "Add launchers by Drag and Drop or by using the context menu."
msgstr "Adicionar lançamentos por arrastamento ou usando o menu de contexto."

#: package/contents/ui/main.qml:171
#, kde-format
msgid "Hide icons"
msgstr "Esconder os ícones"

#: package/contents/ui/main.qml:171
#, kde-format
msgid "Show hidden icons"
msgstr "Mostrar os ícones escondidos"

#: package/contents/ui/main.qml:285
#, kde-format
msgctxt "@action"
msgid "Add Launcher…"
msgstr "Adicionar um Lançamento…"
