# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kdeplasma-addons package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kdeplasma-addons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-30 08:46+0000\n"
"PO-Revision-Date: 2022-12-31 16:19+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.0\n"

#: package/contents/ui/main.qml:32
#, kde-format
msgid "Night Color Control"
msgstr "Besturing van nachtkleur"

#: package/contents/ui/main.qml:35
#, kde-format
msgid "Night Color is inhibited"
msgstr "Nachtkleur is opgeschort"

#: package/contents/ui/main.qml:38
#, kde-format
msgid "Night Color is unavailable"
msgstr "Nachtkleur is niet beschikbaar"

#: package/contents/ui/main.qml:41
#, kde-format
msgid "Night Color is disabled. Click to configure"
msgstr "Nachtkleur is uitgeschakeld. Klik om te configureren"

#: package/contents/ui/main.qml:44
#, kde-format
msgid "Night Color is not running"
msgstr "Nachtkleur is niet actief"

#: package/contents/ui/main.qml:46
#, kde-format
msgid "Night Color is active (%1K)"
msgstr "Nachtkleur is actief (%1K)"

#: package/contents/ui/main.qml:97
#, kde-format
msgid "&Configure Night Color…"
msgstr "Nachtkleur &configureren…"
