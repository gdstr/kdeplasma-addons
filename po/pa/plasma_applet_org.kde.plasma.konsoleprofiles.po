# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# A S Alam <aalam@users.sf.net>, 2012, 2018.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-05 03:09+0000\n"
"PO-Revision-Date: 2018-11-08 22:45-0800\n"
"Last-Translator: A S Alam <alam.yellow@gmail.com>\n"
"Language-Team: Punjabi <punjabi-users@lists.sf.net>\n"
"Language: pa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: package/contents/ui/konsoleprofiles.qml:62
#, kde-format
msgctxt "@title"
msgid "Konsole Profiles"
msgstr "ਕਨਸੋਲ ਪਰੋਫਾਇਲ"

#: package/contents/ui/konsoleprofiles.qml:82
#, kde-format
msgid "Arbitrary String Which Says Something"
msgstr ""
